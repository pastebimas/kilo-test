<?php

namespace App\Repositories;

use App\Models\User;
use Illuminate\Support\Facades\Hash;

class UserRepository extends AbstractRepository
{
    public function getModelClass(): string
    {
        return User::class;
    }

    public function renew($userId)
    {
        $user = $this->model->firstOrFail($userId);
        $user->role_id = User::ROLE_VERIFIED;
        $user->save();
    }
}
