<?php

namespace App\Repositories;

use App\Models\Transaction;
use Illuminate\Database\Eloquent\Model;
use App\Exceptions\TransactionDeadlockException;
use Illuminate\Support\Facades\DB;

class TransactionsRepository extends AbstractRepository
{

    public function getModelClass(): string
    {
        return Transaction::class;
    }

    /**
     * @param array $data
     * @return Model
     * @throws TransactionDeadlockException
     */
    public function save($data): Model
    {
        return DB::transaction(function () use ($data) {
            return parent::save($data);
        }, 3);
    }

}
