<?php

namespace App\Repositories;

use App\Category;
use App\Models\PaymentNotifications;
use App\SubCategory;
use Carbon\Carbon;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Support\Collection;

class PaymentsNotificationsRepository extends AbstractRepository
{

    public function getModelClass(): string
    {
        return PaymentNotifications::class;
    }

}
