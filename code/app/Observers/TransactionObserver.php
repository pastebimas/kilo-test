<?php

namespace App\Observers;


use App\Models\Transaction;
use App\Models\User;
use App\Repositories\UserRepository;

class TransactionObserver
{
    private $userRepository;

    /**
     * TransactionObserver constructor.
     */
    public function __construct(UserRepository $userRepository)
    {
        $this->userRepository = $userRepository;
    }

    public function created(Transaction $transaction)
    {
        switch ($transaction->status) {
            case Transaction::TYPE_INITIAL:
                $userData = []; // todo: find out real user data
                //$this->userRepository->save($userData);
                User::factory(1)->create(); // todo: set USER::ROLE_VERIFIED
                break;
            case Transaction::TYPE_RENEWED:
                $this->userRepository->renew($transaction->user_id);

        }
    }


    public function updated(Transaction $transaction)
    {
        //todo: renew users subscription if transactions are updated
    }
}
