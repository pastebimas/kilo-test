<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Transaction extends Model
{
    use HasFactory;

    CONST TYPE_PENDING = "0";
    CONST TYPE_INITIAL = "1";
    CONST TYPE_RENEWED = "2";
    CONST TYPE_UNSUCCESSFUL = "3";
    CONST TYPE_CANCEL = "4";

}
