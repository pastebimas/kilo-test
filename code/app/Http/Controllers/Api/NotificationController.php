<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Services\PaymentsService;
use Illuminate\Http\Request;

class NotificationController extends Controller
{
    private $paymentsService;

    /**
     * NotificationController constructor.
     */
    public function __construct(PaymentsService $paymentsService)
    {
        $this->paymentsService = $paymentsService;
    }

    public function index(Request $request)
    {
        $paymentPlatform = $this->paymentsService->getPlatform($request->apiType);

        $paymentPlatform->handlePayment($request);
    }

}
