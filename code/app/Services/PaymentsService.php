<?php


namespace App\Services;


use App\Exceptions\PaymentProviderNotFound;
use App\Models\Discount;
use App\Models\Product;
use App\Models\ProductGroup;
use App\Models\ShoppingCart;
use App\Payments\ApplePay;
use App\Payments\PaymentsProviderInterface;
use App\Payments\Stripe;
use App\Services\Shipping\Deutschepost;
use App\Services\Shipping\LithuaniaPost;
use App\Shipping;
use Cookie;
use Illuminate\Support\Facades\App;

class PaymentsService
{

    const PAYMENT_PLATFORM_APPLEPAY = "apple-pay";
    const PAYMENT_PLATFORM_STRIPE = "stripe";
    protected static $cartModel;
    protected $cookieKey = 'cartHash';

    /**
     * @param string $platformKey
     * @return PaymentsProviderInterface
     * @throws PaymentProviderNotFound
     */
    public function getPlatform(string $platformKey)
    {
        switch ($platformKey) {
            case self::PAYMENT_PLATFORM_APPLEPAY:
                App::bind('App\Payments\PaymentsProviderInterface', 'App\Payments\ApplePay');
                break;
            case self::PAYMENT_PLATFORM_STRIPE:
                App::bind('App\Payments\PaymentsProviderInterface', function () {
                    return new Stripe();
                });
                break;
            default:
                throw new PaymentProviderNotFound();

        }

        return App::make('App\Payments\PaymentsProviderInterface');
    }

}
