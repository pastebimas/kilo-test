<?php

namespace App\Payments;

use App\Payments\PaymentsProviderInterface;

class Stripe extends PaymentsAbstraction implements PaymentsProviderInterface
{

    public function auth()
    {

    }

    public function saveReceipt()
    {

    }

    public function saveNotification()
    {

    }
}
