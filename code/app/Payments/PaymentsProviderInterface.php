<?php

namespace App\Payments;

interface PaymentsProviderInterface
{
    public function auth();
    public function handlePayment($request);
    public function saveNotification($request);
    public function saveTransaction($transaction, $notification = null);

}
