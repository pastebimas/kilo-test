<?php

namespace App\Payments;

use App\Models\Transaction;
use App\Repositories\PaymentsNotificationsRepository;
use App\Repositories\TransactionsRepository;

class ApplePay extends PaymentsAbstraction implements PaymentsProviderInterface
{

    private $paymentsNotificationsRepository;
    private $transactionsRepository;

    /**
     * ApplePay constructor.
     */
    public function __construct(PaymentsNotificationsRepository $paymentsNotificationsRepository, TransactionsRepository $transactionsRepository)
    {
        $this->paymentsNotificationsRepository = $paymentsNotificationsRepository;
        $this->transactionsRepository = $transactionsRepository;
    }

    public function auth()
    {

    }

    public function handlePayment($request)
    {
        $notification = $this->saveNotification($request);
        $transaction = $this->saveTransaction($request, $notification);
    }


    public function saveNotification($request)
    {
        $preparedData = [
            'user_id' => $request->bid,
            'adam_id' => $request->auto_renew_adam_id,
            'product_id' => $request->auto_renew_product,
            'status' => $request->auto_renew_status,
            'status_date' => $request->auto_renew_status_change_date,
            'type' => $request->notifiation_type
        ];

        return $this->paymentsNotificationsRepository->save($preparedData);
    }

    public function saveTransaction($request, $notification = null)
    {
        $preparedData = [
            'user_id' => $request->bid,
            'product_id' => $request->latest_receipt_info['product_id'],
            // todo: save prices in products table & retrieve value by latest_receipt_info->product_id
            'price' => $request->latest_receipt_info['quantity'] * 55,
            'status' => $this->mappedStatus($request->notifiation_type),
            'notification_id' => $notification->id
        ];

        return $this->transactionsRepository->save($preparedData);
    }

    private function mappedStatus($status)
    {
        switch ($status) {
            case "CANCEL":
                return Transaction::TYPE_CANCEL;
            case "INITIAL_BUY":
                return Transaction::TYPE_INITIAL;
            case "RENEWAL":
                return Transaction::TYPE_RENEWED;
            case "DID_FAIL_TO_RENEW":
                return Transaction::TYPE_UNSUCCESSFUL;
        }
    }
}
